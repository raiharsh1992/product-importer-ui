var sessionInfo;
var roomId = 0;
var data;
var currentPage = 1;
var currentPageSize = 100;
var viewFilter = 2;
var searchFor = "";
$(document).ready(function() {
  if (parseInt(localStorage.getItem("rememberMe")) == 1) {
    sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
  } else {
    sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
  }
  displayProducts(currentPageSize, currentPage);
});

$(document).on("change", '#viewFilter', function (event) {
  viewFilter = parseInt($(this).children(":selected").attr("value"));
  console.log(viewFilter);
  currentPage = 1;
  if(viewFilter==2){
    displayProducts(currentPageSize, currentPage);
  }
  else{
    displayProductsFilters(currentPageSize, currentPage);
  }
});

$(document).on("change", '#viewSearchCategory', function (event) {
  searchFor = $(this).children(":selected").attr("value");
});

function performSearch() {
  if(searchFor==""){
    M.toast({
      html: 'Kindly select a search category!'
    });
  }
  else{
    if(document.getElementById("searchBar").value!=""){
      currentPage = 1;
      viewFilter = 3;
      displayProductsSearch(currentPageSize, currentPage);
    }
    else{
      M.toast({
        html: 'Kindly pass a search string!'
      });
    }
  }
}

function displayProducts(pageSize, pageCount) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["pageSize"] = pageSize;
  myObj["pageCount"] = pageCount;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'viewproducts/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuthenticate
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        data = usingData;
        var lPrint = '';
        $.each(usingData, function(index, v) {
          var imgUrl = "img/delete.png";
          if (v.isActive) {
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:editProductInfo(" + v.productId + ")'>Edit</a></td><td><div class='switch'><label>Off<input type='checkbox' checked onclick='changeStatusOn(" + v.productId + ")'><span class='lever'></span>On</label></div></td><td><a href='javascript:deleteSelectedProd(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>";
          } else {
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:editProductInfo(" + v.productId + ")'>Edit</a></td><td><div class='switch'><label>Off<input type='checkbox'  onclick='changeStatusOff(" + v.productId + ")'><span class='lever'></span>On</label></div></td><td><a href='javascript:deleteSelectedProd(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>" ;
          }
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalProductCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
        document.getElementById("deleteButton").style.display = "inline-block";
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function displayProductsFilters(pageSize, pageCount) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["pageSize"] = pageSize;
  myObj["pageCount"] = pageCount;
  myObj["isActive"] = viewFilter;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'filter/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuthenticate
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        data = usingData;
        var lPrint = '';
        $.each(usingData, function(index, v) {
          var imgUrl = "img/delete.png";
          if (v.isActive) {
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:editProductInfo(" + v.productId + ")'>Edit</a></td><td><div class='switch'><label>Off<input type='checkbox' checked onclick='changeStatusOn(" + v.productId + ")'><span class='lever'></span>On</label></div></td><td><a href='javascript:deleteSelectedProd(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>";
          } else {
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:editProductInfo(" + v.productId + ")'>Edit</a></td><td><div class='switch'><label>Off<input type='checkbox'  onclick='changeStatusOff(" + v.productId + ")'><span class='lever'></span>On</label></div></td><td><a href='javascript:deleteSelectedProd(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>" ;
          }
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalProductCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
        document.getElementById("deleteButton").style.display = "inline-block";
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function displayProductsSearch(pageSize, pageCount) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["pageSize"] = pageSize;
  myObj["pageCount"] = pageCount;
  myObj["searchIn"] = searchFor;
  myObj["searchKey"] = document.getElementById("searchBar").value;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'search/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuthenticate
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        data = usingData;
        var lPrint = '';
        $.each(usingData, function(index, v) {
          var imgUrl = "img/delete.png";
          if (v.isActive) {
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:editProductInfo(" + v.productId + ")'>Edit</a></td><td><div class='switch'><label>Off<input type='checkbox' checked onclick='changeStatusOn(" + v.productId + ")'><span class='lever'></span>On</label></div></td><td><a href='javascript:deleteSelectedProd(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>";
          } else {
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:editProductInfo(" + v.productId + ")'>Edit</a></td><td><div class='switch'><label>Off<input type='checkbox'  onclick='changeStatusOff(" + v.productId + ")'><span class='lever'></span>On</label></div></td><td><a href='javascript:deleteSelectedProd(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>" ;
          }
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalProductCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
        document.getElementById("deleteButton").style.display = "inline-block";
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function changeStatusOn(catId) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["productId"] = catId;
  myObj["newStatus"] = 0;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'productstatus/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
    },
    success: function(response) {
      M.toast({
        html: 'Status update successfully :)'
      });
      if(viewFilter==2){
        displayProducts(currentPageSize, currentPage);
      }
      else if(viewFilter==3){
        displayProductsSearch(currentPageSize, currentPage);
      }
      else{
        displayProductsFilters(currentPageSize, currentPage);
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}


function changeStatusOff(catId) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["productId"] = catId;
  myObj["newStatus"] = 1;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'productstatus/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
    },
    success: function(response) {
      M.toast({
        html: 'Status update successfully :)'
      });
      if(viewFilter==2){
        displayProducts(currentPageSize, currentPage);
      }
      else if(viewFilter==3){
        displayProductsSearch(currentPageSize, currentPage);
      }
      else{
        displayProductsFilters(currentPageSize, currentPage);
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function finalEditProductInformation() {
  var catName = document.getElementById("catName").value;
  var catSku = document.getElementById("catSku").value;
  var catDesc = document.getElementById("catDesc").value;
  if (catName != "") {
    if(catSku != ""){
      if(catDesc != ""){
          var myObj = {};
          myObj['userId'] = sessionInfo.sessionInfo.userId;
          myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
          myObj["productId"] = roomId;
          myObj["name"] = catName;
          myObj["sku"] = catSku;
          myObj["desc"] = catDesc;
          var jsonObj = JSON.stringify(myObj);
          var url = beUrl() + 'editproduct/';
          $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: jsonObj,
            headers: {
              "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
            },
            success: function(response) {
              M.toast({
                html: 'Product update successfully :)'
              });
              if(viewFilter==2){
                displayProducts(currentPageSize, currentPage);
              }
              else if(viewFilter==3){
                displayProductsSearch(currentPageSize, currentPage);
              }
              else{
                displayProductsFilters(currentPageSize, currentPage);
              }
            },
            error: function(response) {
              var usingInfo = JSON.parse(JSON.stringify(response));
              M.toast({
                html: usingInfo.responseJSON.Data
              });
              if (usingInfo.status == 401) {
                logoutModule();
              }
            }
          });
      }
      else{
        M.toast({
          html: 'Product desc cannot be blank!'
        });
      }
    }
    else{
      M.toast({
        html: 'Product SKU cannot be blank!'
      });
    }
  } else {
    M.toast({
      html: 'Product name cannot be blank!'
    });
  }
}

function deleteSelectedProd(roomId1) {
  if (window.confirm('You sure want to delete the product ?')) {
    finalDeletionOfProduct(roomId1)
    console.log("Here");
  }
}


function finalDeletionOfProduct(roomId1) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["productId"] = roomId1;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'deleteproduct/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
    },
    success: function(response) {
      M.toast({
        html: 'Product Deleted successfully :)'
      });
      if(viewFilter==2){
        displayProducts(currentPageSize, currentPage);
      }
      else if(viewFilter==3){
        displayProductsSearch(currentPageSize, currentPage);
      }
      else{
        displayProductsFilters(currentPageSize, currentPage);
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function editProductInfo(catId) {
  roomId = catId;
  $.each(data, function(i, v) {
    if (v.productId == catId) {
      document.getElementById("catName").value = v.name;
      document.getElementById("catSku").value = v.sku;
      document.getElementById("catDesc").value = v.desc;
      M.updateTextFields();
    }
  });
  var elems = document.getElementById('modal2');
  var options = {};
  var instances = M.Modal.init(elems, options);
  instances.open();
}

function goToNext() {
    currentPage = parseInt(currentPage)+1;
    console.log(currentPage);
    if(viewFilter==2){
      displayProducts(currentPageSize, currentPage);
    }
    else if(viewFilter==3){
      displayProductsSearch(currentPageSize, currentPage);
    }
    else{
      displayProductsFilters(currentPageSize, currentPage);
    }
}

function goToPrevious() {
    if(parseInt(currentPage)>1){
      currentPage = parseInt(currentPage)-1;
      if(viewFilter==2){
        displayProducts(currentPageSize, currentPage);
      }
      else if(viewFilter==3){
        displayProductsSearch(currentPageSize, currentPage);
      }
      else{
        displayProductsFilters(currentPageSize, currentPage);
      }
    }
}

function deleteAll(){
  if (window.confirm('You sure want to delete the product ?')) {
    finalDeleteAll();
  }
}

function finalDeleteAll() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["deleteType"] = "SOFT";
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'deleteall/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
    },
    success: function(response) {
      M.toast({
        html: 'Product Deleted successfully :)'
      });
      location.reload();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}
