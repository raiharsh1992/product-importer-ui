var sessionInfo;
var roomId = 0;
var data;
var currentPage = 1;
var currentPageSize = 100;
$(document).ready(function() {
  if (parseInt(localStorage.getItem("rememberMe")) == 1) {
    sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
  } else {
    sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
  }
  displayProducts(currentPageSize, currentPage);
});

function displayProducts(pageSize, pageCount) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["pageSize"] = pageSize;
  myObj["pageCount"] = pageCount;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'viewdelproducts/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuthenticate
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        data = usingData;
        var lPrint = '';
        $.each(usingData, function(index, v) {
          var imgUrl = "img/delete.png";
          lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.sku + "</td><td>" + v.desc + "</td><td><a href='javascript:initiateDelete(" + v.productId + ")'><img src=" + imgUrl + " class='tableImage hoverable'></a></td></tr>";
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalProductCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function initiateDelete(roomId1) {
  if (window.confirm('You sure want to delete the product permanently?')) {
    deleteFinalSelectedProd(roomId1)
    console.log("Here");
  }
}


function deleteFinalSelectedProd(roomId1) {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["productId"] = roomId1;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'deleteproductperma/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
    },
    success: function(response) {
      M.toast({
        html: 'Product Deleted successfully :)'
      });
      displayProducts(currentPageSize, currentPage);
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function goToNext() {
    currentPage = parseInt(currentPage)+1;
    console.log(currentPage);
    displayProducts(currentPageSize, currentPage);
}

function goToPrevious() {
    if(parseInt(currentPage)>1){
      currentPage = parseInt(currentPage)-1;
      displayProducts(currentPageSize, currentPage);
    }
}

function deleteAll(roomId1) {
  if (window.confirm('You sure want to delete all the products permanently?')) {
    deleteAllFinalStep(roomId1)
  }
}


function deleteAllFinalStep(){
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  myObj["deleteType"] = "HARD";
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'deleteall/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
    },
    success: function(response) {
      M.toast({
        html: 'Product Deleted successfully :)'
      });
      location.reload();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}
