var sessionInfo;
var productManagement = '<li class="bold"><ul class="collapsible collapsible-accordion sidenavColor"><li class="bold"><a class="collapsible-header waves-effect waves-teal inLineText"> <img src="img/product.png" class="menuManagerIcon" /> <span class="fontSize inLineText">Product Management</span> </a><div class="collapsible-body"><ul class="sidenavColor"><li><a href="createproduct.html" class="inLineText"> <img src="img/create.png" class="menuManagerIcon1"> <span class="fontSize">Create New</span> </a></li><li><a href="uploadproduct.html" class="inLineText"> <img src="img/upload.png" class="menuManagerIcon1"> <span class="fontSize">Upload Products</span> </a></li><li><a href="viewproduct.html" class="inLineText"> <img src="img/view.png" class="menuManagerIcon1"> <span class="fontSize">View All</span> </a></li><li><a href="viewproductdeleted.html" class="inLineText"> <img src="img/deleted.png" class="menuManagerIcon1"> <span class="fontSize">View All Deleted</span> </a></li></ul></div></li></ul></li>';

var logOut = '<li class="bold"><a href="javascript:logoutModule()" class="waves-effect waves-teal inLineText"><img src="img/logout.png" class="menuManagerIcon" /><span class="fontSize">Logout</span></a></li>';

$(document).ready(function() {
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    }
  } else {
    location.replace("login.html");
  }
  menuManager();

  const webSocketBridge = new channels.WebSocketBridge();
  var uslSocket = socketUrl() + 'notifications/';
  webSocketBridge.connect(uslSocket);
  webSocketBridge.listen(function(action, stream) {
    if (action.type == "user.listener") {
      console.log("RESPONSE:", action.productCount);
      var displayString = action.productCount + " " + action.event;
      var loveString = '<span>'+displayString+'</span><button class="btn-flat toast-action" onclick="goToProductView()">View</button>'
      M.toast(
        {
          html: loveString,
          classes: 'rounded',
          displayLength : 20000
        }
      )
    }
  })
});

function goToProductView() {
  location.replace("viewproduct.html");
}

function menuManager() {
  var menuPrint = '';
  menuPrint = menuPrint + productManagement;
  menuPrint = menuPrint + logOut;
  $("#nav-mobile").append(menuPrint);
}

function logoutModule() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "logout/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuthenticate
    },
    success: function(response) {
      M.toast({
        html: 'User logged out successfully'
      });
    },
    error: function(response) {
      M.toast({
        html: 'Issue while logging out user'
      });
    }
  });
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      localStorage.removeItem("adminSessionInfo");
    } else {
      sessionStorage.removeItem("adminSessionInfo");
    }
    localStorage.removeItem("rememberMe");
    localStorage.removeItem("deviceIdentifier");
  } else {
    localStorage.removeItem("adminSessionInfo");
    localStorage.removeItem("deviceIdentifier");
    sessionStorage.removeItem("adminSessionInfo");
  }
  location.replace("login.html")
}
