var sessionInfo;
$(document).ready(function() {
  if (parseInt(localStorage.getItem("rememberMe")) == 1) {
    sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
  } else {
    sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
  }
  $('#catSku').on('focusout',function(){
    checkSkuValidity();
  });
});

function submitDelear() {
  event.preventDefault();
  var catName = document.getElementById("catName").value;
  var catSku = document.getElementById("catSku").value;
  var catDesc = document.getElementById("catDesc").value;
  if (catName != "") {
    if (catSku != "") {
      if (catDesc != "") {
        var myObj = {};
        myObj['name'] = catName;
        myObj['sku'] = catSku;
        myObj['desc'] = catDesc;
        myObj['userId'] = sessionInfo.sessionInfo.userId;
        myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
        var jsonObj = JSON.stringify(myObj);
        var url = beUrl() + "createproduct/";
        $.ajax({
          url: url,
          type: 'POST',
          dataType: 'json',
          processData: false,
          contentType: 'application/json',
          data: jsonObj,
          headers: {
            "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
          },
          success: function(response) {
            M.toast({
              html: 'Product created'
            });
            location.assign('viewproduct.html');
          },
          error: function(response) {
            var usingInfo = JSON.parse(JSON.stringify(response));
            M.toast({
              html: usingInfo.responseJSON.Data
            });
            if (usingInfo.status == 401) {
              logoutModule();
            }
          }
        });
      } else {
        M.toast({
          html: 'Product description cannot be left blank'
        });
      }
    } else {
      M.toast({
        html: 'Product SKU cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Product name cannot be left blank'
    });
  }
}

function checkSkuValidity() {
  var catSku = document.getElementById("catSku").value;
  if(catSku!=""){
    var myObj = {};
    myObj['sku'] = catSku;
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['deviceIdentifier'] = localStorage.getItem("deviceIdentifier");
    var jsonObj = JSON.stringify(myObj);
    var url = beUrl() + "isuniquesku/";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
      },
      success: function(response) {
        M.toast({
          html: 'Unique SKU code provided'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  }
  else{
    M.toast({
      html: 'Product SKU cannot be left blank'
    });
  }
}
