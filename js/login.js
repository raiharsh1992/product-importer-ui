$(document).ready(function() {
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        location.replace("index.html");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        location.replace("index.html");
      }
    }
  }
  $(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
      document.getElementById("password-field1").innerHTML = "lock_open";
    } else {
      input.attr("type", "password");
      document.getElementById("password-field1").innerHTML = "lock_outline";
    }
  });
});

function loginUser() {
  event.preventDefault();
  var userName = document.getElementById("icon_prefix").value;
  var password = document.getElementById("password-field").value;
  var rememberMe = document.getElementById("rememberMe").checked;
  var deviceIdentifier = random_hex_color_code();
  if (userName != "") {
    if (password != "") {
      var myObj = {};
      myObj["userName"] = userName;
      myObj["password"] = password;
      myObj["deviceIdentifier"] = deviceIdentifier;
      var jsonObj = JSON.stringify(myObj);
      $.ajax({
        url: beUrl() + 'login/',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: jsonObj,
        success: function(response) {
          if (rememberMe == true) {
            localStorage.setItem("adminSessionInfo", JSON.stringify(response));
            localStorage.setItem("rememberMe", 1);
            localStorage.setItem("deviceIdentifier",deviceIdentifier);
            location.replace("index.html");
          } else {
            sessionStorage.setItem("adminSessionInfo", JSON.stringify(response));
            localStorage.setItem("rememberMe", 0);
            localStorage.setItem("deviceIdentifier",deviceIdentifier);
            location.replace("index.html");
          }
        },
        error: function(response) {
          var usingInfo = JSON.parse(JSON.stringify(response));
          M.toast({
            html: usingInfo.responseJSON.Data
          });
        }
      });
    } else {
      M.toast({
        html: 'Password cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Username cannot be left blank'
    });
  }
}
