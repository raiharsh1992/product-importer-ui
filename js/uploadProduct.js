var sessionInfo;
var uploading = 0;
var taskId = "";
var isFirst = 1;
var dateTime = "";
$(document).ready(function() {
  if (parseInt(localStorage.getItem("rememberMe")) == 1) {
    sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
  } else {
    sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
  }
  document.getElementById("displayLoading").style.display = "none";
});

function submitDelear() {
  event.preventDefault();
  if (uploading == 0) {
    $("#valueOfDisplayLoading").removeClass("determinate");
    $("#valueOfDisplayLoading").addClass("indeterminate");
    uploading = 1;
    if ($('#uploadBox')[0].files[0]) {
      var file = $('#uploadBox')[0].files[0];
      var loaded = 0;
      var step = 2097152 //1024*1024*2; size of one chunk
      var total = file.size; // total size of file
      var start = 0; // starting position
      var reader = new FileReader();
      var blob = file.slice(start, step); //a single chunk in starting of step size
      reader.readAsBinaryString(blob); // reading that chunk. when it read it, onload will be invoked
      reader.onload = function(e) {
        document.getElementById("displayLoading").style.display = "block";
        var form = new FormData();
        var isLast = (total-loaded);
        var isFinal = 0;
        if(isLast<=step){
          isFinal=1;
        }
        form.append("userId", sessionInfo.sessionInfo.userId);
        form.append("deviceIdentifier", localStorage.getItem("deviceIdentifier"));
        form.append("isLastChunk",isFinal);
        form.append("isFirstRequest",isFirst);
        form.append("dateTimeRequest",dateTime);
        form.append("file", reader.result);
        var url = beUrl() + "uploadproducts/";
        $.ajax({
          url: url,
          method: 'POST',
          type: 'POST',
          async: true,
          crossDomain: true,
          processData: false,
          contentType: false,
          mimeType: "multipart/form-data",
          data: form,
          headers: {
            "basicAuthenticate": sessionInfo.sessionInfo.basicAuthenticate,
          },
          error: function(response) {
            uploading = 0;
            var usingInfo = JSON.parse(JSON.stringify(response));
            M.toast({
              html: usingInfo.responseJSON.Data
            });
            document.getElementById("displayLoading").style.display = "none";
            if (usingInfo.status == 401) {
              logoutModule();
            }
          }
        }).done(function(data) {
          loaded += step;
          if(loaded<=total){
            blob = file.slice(loaded,loaded+step);  // getting next chunk
            isFirst = 0;
            var usingInfo = JSON.parse(data);
            dateTime = usingInfo.dateTime;
            reader.readAsBinaryString(blob);
          }
          else{
            isFirst = 1;
            var usingInfo = JSON.parse(data);
            var taskId = usingInfo.taskid;
            console.log(taskId);
            M.toast({
              html: 'File uploaded successfully, once processing is completed we will notify you',
              //completeCallback: function(){location.assign('index.html');},
              displayLength: 6000
            });
            checkInsertionProgress(taskId);
          }
        });
      };
    } else {
      M.toast({
        html: 'Kindly select a CSV file to upload'
      });
    }
  } else {
    M.toast({
      html: 'Please be paitent a file is being uploaded'
    });
  }
}

function checkInsertionProgress(taskId) {
  var url = beUrl() + 'get_progress/';
  var settings = {
    "url": url,
    "method": "POST",
    "timeout": 0,
    "data": JSON.stringify({
      "task_id": taskId
    }),
  };
  $.ajax(settings).done(function(response) {
    console.log(response);
    var usingInfo = JSON.parse(JSON.stringify(response));
    if (usingInfo.Data.state == "SUCCESS") {
      uploading = 0;
      document.getElementById("displayLoading").style.display = "none";
    }
    if (usingInfo.Data.state == "PROGRESS") {
      $("#valueOfDisplayLoading").addClass("determinate");
      $("#valueOfDisplayLoading").removeClass("indeterminate");
      $("#valueOfDisplayLoading").css("width", usingInfo.Data.details.percent + "%");
      setTimeout(checkInsertionProgress(taskId), 250000);
    }
    if (usingInfo.Data.state == "PENDING") {
      setTimeout(checkInsertionProgress(taskId), 250000);
    }
  });

}

function processData(allText) {
    var record_num = 5;  // or however many elements there are in each row
    var allTextLines = allText.split(/\r\n|\n/);
    var entries = allTextLines[0].split(',');
    var lines = [];

    var headings = entries.splice(0,record_num);
    while (entries.length>0) {
        var tarr = [];
        for (var j=0; j<record_num; j++) {
            tarr.push(headings[j]+":"+entries.shift());
        }
        lines.push(tarr);
    }
    console.log(lines);
}

function toUTF8Array(str) {
    var utf8 = [];
    for (var i=0; i < str.length; i++) {
        var charcode = str.charCodeAt(i);
        if (charcode < 0x80) utf8.push(charcode);
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6),
                      0x80 | (charcode & 0x3f));
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12),
                      0x80 | ((charcode>>6) & 0x3f),
                      0x80 | (charcode & 0x3f));
        }
        // surrogate pair
        else {
            i++;
            // UTF-16 encodes 0x10000-0x10FFFF by
            // subtracting 0x10000 and splitting the
            // 20 bits of 0x0-0xFFFFF into two halves
            charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                      | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >>18),
                      0x80 | ((charcode>>12) & 0x3f),
                      0x80 | ((charcode>>6) & 0x3f),
                      0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}
